<?php
	include_once("../config.php");
	include_once("../methods.php");
	//$token = $_GET["m"];
	$send_to = "";	
	$user_id = "";
	$imageid = "--";
	$previousjson = "[]";
	$isediting = 0;
	$titleimg = "";
	
	$wh = "1#1";
	$widthheight = explode("#",$wh);	
	
	if(isset($_GET["flockEvent"])){
		$response = $_GET["flockEvent"];
		$decode_json = json_decode($response, true);
		$request_type = $decode_json["name"];
		if($request_type=="client.pressButton" || $request_type=="client.slashCommand"){
			$user_id = $decode_json["userId"];
			$send_to = $decode_json["chat"];
			
			if(isset($_GET["edit"]) && $_GET["edit"]=="Yes"){
				$imageid = $decode_json["buttonId"];
				
				$getjson = "SELECT json, title, uid, public, wh FROM images WHERE id ='$imageid'";
				$jsonrs = mysqli_query($con, $getjson);
				$row = mysqli_fetch_array($jsonrs);
				$previousjson = $row["json"];
				$titleimg = $row["title"];
				$uid = $row["uid"];
				$public = $row["public"];
				$wh = $row["wh"];
				$widthheight = explode("#",$wh);
				if($public=="Yes"){
					$isediting = 1;
				}	
			}	
		}
	}
?>
<!DOCTYPE html>
<html lang="en" ng-app="kitchensink">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Drawing Pad</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dc.css?v=1.9" rel="stylesheet" type="text/css">
    <link href="https://daneden.github.io/animate.css/animate.min.css" rel="stylesheet" type="text/css">
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
  <script src="https://use.fontawesome.com/ab6a0a9f6d.js"></script>
 
    <style>
      pre { margin-left: 15px !important }
	  #temcon,#text-wrapper,#freecon,#sharecon,#imagecon{ display:none;}
    </style>
    <!--[if lt IE 9]>
      <script src="../lib/excanvas.js"></script>
    <![endif]-->

     <script>
      (function() {
        var fabricUrl = 'js/fabric.js';
        if (document.location.search.indexOf('load_fabric_from=') > -1) {
          var match = document.location.search.match(/load_fabric_from=([^&]*)/);
          if (match && match[1]) {
            fabricUrl = match[1];
          }
        }
        document.write('<script src="' + fabricUrl + '"><\/script>');
      })();
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.6/angular.min.js"></script>
  
  </head>
  <body>
<link rel="stylesheet" href="css/kess.css">

<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Plaster' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Engagement' rel='stylesheet' type='text/css'>


<div  ng-controller="CanvasControls">

<div id="allbodyman">
	<div id="sidebar">
    	<ul id="lbar">
            <!-- <li id="litemp" ng-click="setFreeDrawingMode(0)" style="padding-top:20px;"><a href="#"><i class="fa fa-picture-o"></i></a></li>-->
            <li id="litext" ng-click="setFreeDrawingMode(0)"><a href="#"><i class="fa fa-text-width"></i></a></li>
            <li id="liimage" ng-click="setFreeDrawingMode(0)"><a href="#"><i class="fa fa-file-image-o"></i><br></a></li>
            <li id="lishape" ng-click="setFreeDrawingMode(0)"><a href="#"><i class="glyphicon glyphicon-triangle-top"></i><br></a></li> 
            <li id="lifree" ng-click="setFreeDrawingMode(1)"><a href="#"><i class="fa fa-pencil"></i><br></a></li> 
            <li id="lishare" ng-click="setFreeDrawingMode(0)"><a href="#"><i class="fa fa-smile-o"></i><br></a></li> 
            <li id="lishare"><div ng-click="sendBackwards()"><input type="color" bind-value-to="canvasBgColor"></div></li> 
            
		</ul>
    </div>
	<div class="col-md-2" id="leftpan">
     	<div id="temcon"><br> 
        		<button type="button" class="btn txtbt" ng-click="addImagea('http://www.christianebelanger-danse.com/centreuriel/wp-content/uploads/2014/12/background-centre-uriel-diffuseur-sp%C3%A9cialis%C3%A9-700x400.jpg')">Image 1 (pug)</button>  <br> <br>    
                 <button type="button" class="btn txtbt" ng-click="addImagea('bg.jpg')">Add template</button>
                  <br> <br><button type="button" class="btn txtbt" ng-click="addImage1('http://engineeringbuddy.in/flock/draw/pad/images/flow-square.png')">Add template</button>
                 <br><br>
        </div>
     	<div id="text-wrapper" align="center">
        <br><br>
        <button class="btn txtbt" ng-click="addText()"><i class="glyphicon glyphicon-plus"></i> Add Text</button>
        </div>
     	<div id="imagecon">
        <div align="center"><h3>Add Image</h3></div><br>
                <label class="fileContainer" id="i_file">
                    <i class="fa fa-picture-o"></i> Browse image
                    <input type="file"/>
                </label><br>
                <div align="center"><img id="prev_img" src="" width="200" style="display:none;" /></div><br>
                 <input type="hidden" id="ite" style="color:#000;" />
                 <button ng-click="addimageexternal()" class="mybtaar" id="uploadbt"><i class="fa fa-plus"></i> Add image</button>
                <br><br>
                
               
        </div>
     	<div id="shapecon" ng-hide="getText()" class="ng-hide">
        <div align="center"><h3>Add Shapes</h3></div><br>
        	<div>	
            	<div class="blocker" ng-click="addRect()"><i class="fa fa-stop"></i></div>
            	<div class="blocker" ng-click="addTriangle()"><i class="glyphicon glyphicon-triangle-top"></i></div>
            	<div class="blocker" ng-click="addCircle()"><i class="fa fa-circle"></i></div>
                <div class="blocker" ng-click="addLine()"><i class="fa fa-minus"></i></div>
                <div class="blocker"  ng-click="addPolygon()"><i class="glyphicon glyphicon-chevron-left"></i></div>
                <div class="blocker"><img crossOrigin="anonymous" src="images/flow-square.png" width="50"  ng-click="addImage1('images/flow-square.png')"></div>
				<div class="blocker"><img crossOrigin="anonymous" src="images/dimond.png" width="50"  ng-click="addImage1('images/dimond.png')"></div>
				<div class="blocker"><img crossOrigin="anonymous" src="images/oval.png" width="50"  ng-click="addImage1('images/oval.png')"></div>
                <div class="blocker"><img crossOrigin="anonymous" src="images/flow_skew.png" width="50"  ng-click="addImage1('images/flow_skew.png')"></div>
				<div class="blocker"><img crossOrigin="anonymous" src="images/line.png" height="50"  ng-click="addImage1('images/line.png')"></div>
				<div class="blocker"><img crossOrigin="anonymous" src="images/flowchart_arrow.png" height="50"  ng-click="addImage1('images/flowchart_arrow.png')"></div>
                


            </div>
        	<div style="clear:both;">&nbsp;</div>
        <div id="color-opacity-controls" ng-show="canvas.getActiveObject()">
	<br>
    <label for="color" style="margin-left:10px">Shape Color: </label>
    <input type="color" style="width:40px" bind-value-to="fill">
    <br><br>
    <label for="opacity">&nbsp;&nbsp;&nbsp;Opacity: </label>
    <input value="100" type="range" bind-value-to="opacity">
 </div>
        
        </div>
     	<div id="freecon">
        	<div id="drawing-mode-wrapper">

        <!--<button id="drawing-mode" class="btn btn-info"
          ng-click="setFreeDrawingMode(!getFreeDrawingMode())"
          ng-class="{'btn-inverse': getFreeDrawingMode()}">
          {[ getFreeDrawingMode() ? 'Exit free drawing mode' : 'Enter free drawing mode' ]}
        </button>
-->			<div align="center"><h3>Free Drawing</h3></div><br>
        <div ng-show="getFreeDrawingMode()">
          <label for="drawing-mode-selector">Mode:</label>
          <select id="drawing-mode-selector" bind-value-to="drawingMode">
            <option>Pencil</option>
            <option>Circle</option>
            <option>Spray</option>
            <option>Pattern</option>

            <option>hline</option>
            <option>vline</option>
            <option>square</option>
            <option>diamond</option>
            <option>texture</option>
          </select>
          <br><br>
          <label for="drawing-color">Line color:</label>
          <input type="color" value="#005E7A" bind-value-to="drawingLineColor">
          <br><br>
          <label for="drawing-line-width">Line width:</label>
          <input type="range" value="30" min="0" max="150" bind-value-to="drawingLineWidth">
          <br> <br>
          <label for="drawing-shadow-width">Line shadow width:</label>
          <input type="range" value="0" min="0" max="50" bind-value-to="drawingLineShadowWidth">
        </div>
      </div>
        </div>
     	<div id="sharecon">
        	 
             <div>	
            	<div class="blocker"><img crossOrigin="anonymous" src="images/1.png" width="70"  ng-click="addImage1('images/1.png')"></div>
                
            	<div class="blocker"><img crossOrigin="anonymous" src="images/2.png" width="70"  ng-click="addImage1('images/2.png')"></div>
            	<div class="blocker"><img crossOrigin="anonymous" src="images/3.png" width="70"  ng-click="addImage1('images/2.png')"></div>
            	<div class="blocker"><img crossOrigin="anonymous" src="images/4.png" width="70"  ng-click="addImage1('images/2.png')"></div>
				
               </div>
             <div style="clear:both;">&nbsp;</div>
             
             
        </div>
     
     
    <div ng-show="getText()" id="textcontpan"><br>
                    <textarea bind-value-to="text" class="myform"></textarea>
                	<div><br>
                      <button type="button" class="btn btn-object-action"
                        ng-click="toggleBold()"
                        ng-class="{'btn-inverse': isBold()}">
                        <i class="fa fa-bold"></i>
                      </button>
                      <button type="button" class="btn btn-object-action" id="text-cmd-italic"
                        ng-click="toggleItalic()"
                        ng-class="{'btn-inverse': isItalic()}">
                        <i class="fa fa-italic"></i>
                      </button>
                      <button type="button" class="btn btn-object-action" id="text-cmd-underline"
                        ng-click="toggleUnderline()"
                        ng-class="{'btn-inverse': isUnderline()}">
                        <i class="fa fa-underline"></i>
                      </button>
                      <button type="button" class="btn btn-object-action" id="text-cmd-linethrough"
                        ng-click="toggleLinethrough()"
                        ng-class="{'btn-inverse': isLinethrough()}">
                        <i class="fa fa-strikethrough"></i>
                      </button>
                      
                    </div>
                    <div>
                      <label for="font-family">Font family:</label>
                      <select id="font-family" class="btn-object-action" bind-value-to="fontFamily">
                        <option value="Lobster">Arial</option>
                        <option value="helvetica" selected>Helvetica</option>
                        <option value="myriad pro">Myriad Pro</option>
                        <option value="delicious">Delicious</option>
                        <option value="verdana">Verdana</option>
                        <option value="georgia">Georgia</option>
                        <option value="courier">Courier</option>
                        <option value="comic sans ms">Comic Sans MS</option>
                        <option value="impact">Impact</option>
                        <option value="monaco">Monaco</option>
                        <option value="optima">Optima</option>
                        <option value="hoefler text">Hoefler Text</option>
                        <option value="plaster">Plaster</option>
                        <option value="engagement">Engagement</option>
                      </select>
                      <br>
                      <!--<label for="text-align" style="display:inline-block">Text align:&nbsp;&nbsp;</label>
                      <select id="text-align" class="btn-object-action" bind-value-to="textAlign">
                        <option>Left</option>
                        <option>Center</option>
                        <option>Right</option>
                        <option>Justify</option>
                      </select>
                      --><div>
                        <label for="text-bg-color">Color:</label>
                         <input type="color" style="width:40px" bind-value-to="fill" class="btn-object-action">
                      </div>
                      <div>
                        <label for="text-lines-bg-color">Background text color:</label>
                        <input type="color" value="" id="text-lines-bg-color" size="10" class="btn-object-action"
                          bind-value-to="textBgColor">
                      </div>
                      <div>
                        <label for="text-stroke-color">Stroke color:</label>
                        <input type="color" value="" id="text-stroke-color" class="btn-object-action"
                          bind-value-to="strokeColor">
                      </div>
                      <div>
                        <label for="text-stroke-width">Stroke width:</label>
                        <input type="range" value="1" min="1" max="5" id="text-stroke-width" class="btn-object-action"
                          bind-value-to="strokeWidth">
                      </div>
                      <div>
                        <label for="text-font-size">Font size:</label>
                        <input type="range" value="" min="1" max="120" step="1" id="text-font-size" class="btn-object-action"
                          bind-value-to="fontSize">
                      </div>
                      <div>
                        <label for="text-line-height">Line height:</label>
                        <input type="range" value="" min="0" max="10" step="0.1" id="text-line-height" class="btn-object-action"
                          bind-value-to="lineHeight">
                      </div>
                    </div>
                    
        
        
        
        </div>
           
            
    </div>
	<div id="rightpan">
    	<div id="cana"><canvas id="canvas" width="<?php echo $widthheight[0]; ?>" height="<?php echo $widthheight[1]; ?>"></canvas></div>
    </div>

</div> 
<div id="bottomcontrol">
					<div id="senddatap" ng-click="saveJSON()">Send</div>
					<div id="prebt" data-toggle="modal" data-target="#myModal">Preview</div>
	                <div ng-click="sendBackwards()"><img src="images/send-backward.png" width="30px"></div>
    	            <div ng-click="sendToBack()"><img src="images/send-back.png" width="30px"></div>
        		    <div ng-click="bringForward()"><img src="images/send-forward.png" width="30px"></div>
        		    <div ng-click="bringToFront()"><img src="images/bring-front.png" width="30px"></div>
        		    <div ng-click="shadowify()"><img src="images/shadow.png" width="30px"></div>
        		    <div ng-click="confirmClear()" style="background-color:#b71c1c">Clear All</div>
        		    <div ng-click="removeSelected()"><i class="fa fa-trash-o"></i></div>
                    <div id="zoomplus">+</div>
                    <div id="z" ng-click="loadJSON()">Zoom</div>
                    <div id="zoomminus">-</div>
                	
    </div>
    

</div>

<div>
	<textarea id="old_image_json" style="display: none;"><?php echo $previousjson; ?></textarea>
</div>
 
<div class="modal fade" id="create_img" role="dialog">
    <div class="modal-dialog modal-sm" style="width:300px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Image</h4>
        </div>
        <div class="modal-body"> 
          <input type="text" class="form-control" placeholder="Width" id="width_can"><br>
          <input type="text" class="form-control" placeholder="Height" id="height_can">
        </div>
        <div class="modal-footer">
          <button type="button" id="create_image_bt" class="btn btn-success" data-dismiss="modal">Create</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="share_img" role="dialog">
    <div class="modal-dialog modal-sm" style="width:300px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Image</h4>
        </div>
        <div class="modal-body"> 
          <input type="text" class="form-control" placeholder="Title" id="img_title" value="<?php echo $titleimg; ?>"><br>
          <input type="text" class="form-control" placeholder="Description" id="img_desc">
          <input type="text" id="img_json" style="display: none;">
         <br>
          <div <?php if($user_id!=$uid && $isediting =="1"){echo 'style="display:none;"';} ?>>Team Editing: <div style="margin-top:10px;"> 
          <label><input type="radio" name="ispublic" value="Yes" checked/> Yes</label><label><input type="radio" name="ispublic" value="No"/> No</label></div>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" id="send_img_group" class="btn btn-success">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <img src="#" id="impre" crossOrigin="anonymous">
        <div id="imgshow"></div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/jquery.mousewheel.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src=" https://apps-static.flock.co/js-sdk/0.1.0/flock.js"></script>
    
    
<!-- <script src="http://fabricjs.com/../lib/centering_guidelines.js"></script>
<script src="../../lib/aligning_guidelines.js"></script> -->

<script src="js/font-define.js"></script>

<script>
  var kitchensink = { };
  var canvas = new fabric.Canvas('canvas');
</script>

<script src="js/utils.js"></script>
<script src="js/app_config.js"></script>
<script src="js/cont1.js?v=7"></script>

<script>
var wh;

  (function() {

    if (document.location.hash !== '#zoom') return;

    function renderVieportBorders() {
      var ctx = canvas.getContext();

      ctx.save();

      ctx.fillStyle = 'rgba(0,0,0,0.1)';

      ctx.fillRect(
        canvas.viewportTransform[4],
        canvas.viewportTransform[5],
        canvas.getWidth() * canvas.getZoom(),
        canvas.getHeight() * canvas.getZoom());

      ctx.setLineDash([5, 5]);

      ctx.strokeRect(
        canvas.viewportTransform[4],
        canvas.viewportTransform[5],
        canvas.getWidth() * canvas.getZoom(),
        canvas.getHeight() * canvas.getZoom());

      ctx.restore();
    }

    $(canvas.getElement().parentNode).on('mousewheel', function(e) {

      var newZoom = canvas.getZoom() + e.deltaY / 300;
      canvas.zoomToPoint({ x: e.offsetX, y: e.offsetY }, newZoom);

      renderVieportBorders();

      return false;
    });

    var viewportLeft = 0,
        viewportTop = 0,
        mouseLeft,
        mouseTop,
        _drawSelection = canvas._drawSelection,
        isDown = false;

    canvas.on('mouse:down', function(options) {
      isDown = true;

      viewportLeft = canvas.viewportTransform[4];
      viewportTop = canvas.viewportTransform[5];

      mouseLeft = options.e.x;
      mouseTop = options.e.y;

      if (options.e.altKey) {
        _drawSelection = canvas._drawSelection;
        canvas._drawSelection = function(){ };
      }
      renderVieportBorders();
});

    canvas.on('mouse:move', function(options) {
      if (options.e.altKey && isDown) {
        var currentMouseLeft = options.e.x;
        var currentMouseTop = options.e.y;

        var deltaLeft = currentMouseLeft - mouseLeft,
            deltaTop = currentMouseTop - mouseTop;

        canvas.viewportTransform[4] = viewportLeft + deltaLeft;
        canvas.viewportTransform[5] = viewportTop + deltaTop;

        canvas.renderAll();
        renderVieportBorders();
      }
    });

    canvas.on('mouse:up', function() {
      canvas._drawSelection = _drawSelection;
      isDown = false;
    });
  })();

</script>

    </div>

    <script>
      (function(){
        var mainScriptEl = document.getElementById('main');
        if (!mainScriptEl) return;
        var preEl = document.createElement('pre');
        var codeEl = document.createElement('code');
        codeEl.innerHTML = mainScriptEl.innerHTML;
        codeEl.className = 'language-javascript';
        preEl.appendChild(codeEl);
        document.getElementById('bd-wrapper').appendChild(preEl);
      })();
    </script>

    <script>
(function() {
  fabric.util.addListener(fabric.window, 'load', function() {
    var canvas = this.__canvas || this.canvas,
        canvases = this.__canvases || this.canvases;

    canvas && canvas.calcOffset && canvas.calcOffset();

    if (canvases && canvases.length) {
      for (var i = 0, len = canvases.length; i < len; i++) {
        canvases[i].calcOffset();
      }
    }
  });
})();
</script>
<script>
$( document ).ready(function() {
	
	var iseditimg = "<?php echo $isediting; ?>";
	
	if(iseditimg=="0"){
		$('#create_img').modal('show');
	}else{
		$("#z").click();
	}
		
if($('#shapecon').is(':visible')) {
	$('#litemp').removeClass('liactive');
	$('#litext').removeClass('liactive');
	$('#liimage').removeClass('liactive');
	$('#lishape').addClass('liactive');
	$('#lifree').removeClass('liactive');
	$('#lishare').removeClass('liactive');
}
$("#allbodyman").mouseover(function() {
	if($('#textcontpan').is(':visible')) {
		$('#litemp').removeClass('liactive');
		$('#litext').addClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').removeClass('liactive');
	}
if($('#shapecon').is(':visible')) {
	$('#litemp').removeClass('liactive');
	$('#litext').removeClass('liactive');
	$('#liimage').removeClass('liactive');
	$('#lishape').addClass('liactive');
	$('#lifree').removeClass('liactive');
	$('#lishare').removeClass('liactive');
}
if($('#temcon').is(':visible')) {
	$('#litemp').addClass('liactive');
	$('#litext').removeClass('liactive');
	$('#liimage').removeClass('liactive');
	$('#lishape').removeClass('liactive');
	$('#lifree').removeClass('liactive');
	$('#lishare').removeClass('liactive');
}
if($('#freecon').is(':visible')) {
	$('#litemp').removeClass('liactive');
	$('#litext').removeClass('liactive');
	$('#liimage').removeClass('liactive');
	$('#lishape').removeClass('liactive');
	$('#lifree').addClass('liactive');
	$('#lishare').removeClass('liactive');
}		
});



$( "#lishape" ).click(function() {
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','none');
		$('#text-wrapper').css('display','none');
		$('#freecon').css('display','none');
		$('#sharecon').css('display','none');
		$('#imagecon').css('display','none');
		$('#shapecon').css('display','block');
		
		$('#temcon').removeClass('animated pulse');
		$('#text-wrapper').removeClass('animated pulse');
		$('#freecon').removeClass('animated pulse');
		$('#sharecon').removeClass('animated pulse');
		$('#imagecon').removeClass('animated pulse');
		$('#shapecon').addClass('animated pulse');
		
		$('#litemp').removeClass('liactive');
		$('#litext').removeClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').addClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').removeClass('liactive');
	}
});

$( "#litext" ).click(function() {
	
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','none');
		$('#text-wrapper').css('display','block');
		$('#freecon').css('display','none');
		$('#sharecon').css('display','none');
		$('#imagecon').css('display','none');
		$('#shapecon').css('display','none');
		
		$('#temcon').removeClass('animated pulse');
		$('#text-wrapper').addClass('animated pulse');
		$('#freecon').removeClass('animated pulse');
		$('#sharecon').removeClass('animated pulse');
		$('#imagecon').removeClass('animated pulse');
		$('#shapecon').removeClass('animated pulse');
		
		$('#litemp').removeClass('liactive');
		$('#litext').addClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').removeClass('liactive');
	}
	
	
	
});

$( "#litemp" ).click(function() {
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','block');
		$('#text-wrapper').css('display','none');
		$('#freecon').css('display','none');
		$('#sharecon').css('display','none');
		$('#imagecon').css('display','none');
		$('#shapecon').css('display','none');
		
		$('#temcon').addClass('animated pulse');
		$('#text-wrapper').removeClass('animated pulse');
		$('#freecon').removeClass('animated pulse');
		$('#sharecon').removeClass('animated pulse');
		$('#imagecon').removeClass('animated pulse');
		$('#shapecon').removeClass('animated pulse');
		
		$('#litemp').addClass('liactive');
		$('#litext').removeClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').removeClass('liactive');
	}
	
	
	
	
	
});

$( "#liimage" ).click(function() {
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','none');
		$('#text-wrapper').css('display','none');
		$('#freecon').css('display','none');
		$('#sharecon').css('display','none');
		$('#imagecon').css('display','block');
		$('#shapecon').css('display','none');
		
		$('#temcon').removeClass('animated pulse');
		$('#text-wrapper').removeClass('animated pulse');
		$('#freecon').removeClass('animated pulse');
		$('#sharecon').removeClass('animated pulse');
		$('#imagecon').addClass('animated pulse');
		$('#shapecon').removeClass('animated pulse');
		
		$('#litemp').removeClass('liactive');
		$('#litext').removeClass('liactive');
		$('#liimage').addClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').removeClass('liactive');
	}
	
});

$( "#lishare" ).click(function() {
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','none');
		$('#text-wrapper').css('display','none');
		$('#freecon').css('display','none');
		$('#sharecon').css('display','block');
		$('#imagecon').css('display','none');
		$('#shapecon').css('display','none');
		
		$('#temcon').removeClass('animated pulse');
		$('#text-wrapper').removeClass('animated pulse');
		$('#freecon').removeClass('animated pulse');
		$('#sharecon').addClass('animated pulse');
		$('#imagecon').removeClass('animated pulse');
		$('#shapecon').removeClass('animated pulse');
		
		$('#litemp').removeClass('liactive');
		$('#litext').removeClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').removeClass('liactive');
		$('#lishare').addClass('liactive');
	}
});

$( "#lifree" ).click(function() {
	if($(this).hasClass("liactive")){
		$('#leftpan').css('display','none');
		$(this).removeClass('liactive');
	}else{
		$('#leftpan').css('display','block');
		$('#temcon').css('display','none');
		$('#text-wrapper').css('display','none');
		$('#freecon').css('display','block');
		$('#sharecon').css('display','none');
		$('#imagecon').css('display','none');
		$('#shapecon').css('display','none');
		
		$('#temcon').removeClass('animated pulse');
		$('#text-wrapper').removeClass('animated pulse');
		$('#freecon').addClass('animated pulse');
		$('#sharecon').removeClass('animated pulse');
		$('#imagecon').removeClass('animated pulse');
		$('#shapecon').removeClass('animated pulse');
		
		$('#litemp').removeClass('liactive');
		$('#litext').removeClass('liactive');
		$('#liimage').removeClass('liactive');
		$('#lishape').removeClass('liactive');
		$('#lifree').addClass('liactive');
		$('#lishare').removeClass('liactive');
	}
});

$( "#prebt" ).click(function() {
		var dataURL = canvas.toDataURL();
		/*var img = new Image();
		img.id = "impre"
		img.setAttribute('crossOrigin', 'anonymous');
		img.src = dataURL;
		document.getElementById('imgshow').appendChild(img);*/
        document.getElementById('impre').src = dataURL;
	  
});
});





$( "#senddatap123" ).click(function() {
	  
	    var dataString="m="+canvas.toDataURL('png')+"&s=u:4in343343nwl3w33";
		console.log(dataString);
		$.ajax({
			type:'POST',
			data:dataString,
			cache: false,
			contentType: "application/x-www-form-urlencoded",
			url:'https://engineeringbuddy.in/flock/draw/pad/scripts/send.php',
			beforeSend: function(){
				$("#senddatap").text("sending");
			},
			success:function(data) {
				$("#senddatap").append(data);
				//alert(data);
			},error: function(){
				$("#senddatap").text("error");
			}
		});
	  
	});


	function base64ToBlob(base64, mime) 
	{
		mime = mime || '';
		var sliceSize = 1024;
		var byteChars = window.atob(base64);
		var byteArrays = [];
	
		for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
			var slice = byteChars.slice(offset, offset + sliceSize);
	
			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}
	
			var byteArray = new Uint8Array(byteNumbers);
	
			byteArrays.push(byteArray);
		}
	
		return new Blob(byteArrays, {type: mime});
	}
	
	$( "#senddatap" ).click(function() {
		
		$('#share_img').modal('show');
	  	/*var send_to = '';
		var image = canvas.toDataURL('png');
		var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
		var blob = base64ToBlob(base64ImageContent, 'image/png');                
		var formData = new FormData();
		formData.append('m', blob);
		formData.append('s', send_to);
	  
	    //var dataString="m="+canvas.toDataURL('png')+"&s=u:4in343343nwl3w33";
		console.log(blob);
		$.ajax({
			type:'POST',
			data:formData,
			cache: false,
			contentType: false,
			processData: false,
			url:'https://engineeringbuddy.in/flock/draw/pad/scripts/send.php',
			beforeSend: function(){
				$("#senddatap").text("sending");
			},
			success:function(data) {
				$("#senddatap").append(data);
				alert(data);
				
			},error: function(){
				$("#senddatap").text("error");
			}
		});*/
	
		  
	});

</script>
    <script>


$('#i_file').change( function(event) {
	var tmppath = URL.createObjectURL(event.target.files[0]);
    $("#prev_img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
    $("#ite").val(tmppath);
	$("#uploadbt").css('display','block');
	});
	
var zoom = 1;
$( "#zoomplus" ).click(function() {	
	zoom = zoom + 0.1;
	$("#rightpan").animate({ 'zoom': zoom }, 400);
	
});
$( "#zoomminus" ).click(function() {	
zoom = zoom - 0.1;
	$("#rightpan").animate({ 'zoom': zoom }, 400);
});	

$( "#create_image_bt" ).click(function() {	
	var width = $("#width_can").val();	
	var height = $("#height_can").val();
	canvas.setHeight(height);
	canvas.setWidth(width);
	canvas.renderAll();
	
	wh = width+"#"+height;
	
});	

$( "#send_img_group" ).click(function() {	
	var img_title = $("#img_title").val();	
	var img_desc = $("#img_desc").val();
	var img_json = $("#img_json").val();
	var ispublic = $('input[name="ispublic"]:checked').val();
	
	if(img_title!=""){

		var send_to = '<?php echo $send_to; ?>';
		var user_id = '<?php echo $user_id; ?>';
		var isediting = '<?php echo $isediting; ?>';
		var imageid = '<?php echo $imageid; ?>';
		 
		var image = canvas.toDataURL('png');
		var base64ImageContent = image.replace(/^data:image\/(png|jpg);base64,/, "");
		var blob = base64ToBlob(base64ImageContent, 'image/png');                
		var formData = new FormData();
		formData.append('m', blob);
		formData.append('s', send_to);
		formData.append('title', img_title);
		formData.append('desc', img_desc);
		formData.append('user_id', user_id);
		formData.append('img_json', img_json);
		formData.append('ispublic', ispublic);
		formData.append('isediting', isediting);
		formData.append('imageid', imageid);
		formData.append('imageid', imageid);
		formData.append('wh', wh);
	  
	    //var dataString="m="+canvas.toDataURL('png')+"&s=u:4in343343nwl3w33";
		console.log(blob);
		$.ajax({
			type:'POST',
			data:formData,
			cache: false,
			contentType: false,
			processData: false,
			url:'scripts/send.php',
			beforeSend: function(){
				$("#send_img_group").text("Sending..");
			},
			success:function(data) {
				//$("#send_img_group").append(data);
				$("#send_img_group").text("Sent");
				flock.close();
				
			},error: function(){
				$("#send_img_group").text("Error");
			}
		});
		
	}
	
});	

console.log("img is <?php echo $imageid;?>");


</script>
  </body>
</html>