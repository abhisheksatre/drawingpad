<?php

	include_once("config.php");
	include_once("methods.php");
	//$token = $_GET["m"];
	
	$response = file_get_contents('php://input');
	$decode_json = json_decode($response, true);
	
	$request_type = $decode_json["name"];
	
	if($request_type=="app.install"){
		$user_id = $decode_json["userId"];
		$token = $decode_json["token"];
		addUser($user_id, $token);
	}else if($request_type=="app.uninstall"){
		$user_id = $decode_json["userId"];
		uninstall($user_id);
	}else if($request_type=="client.slashCommand"){
		$user_id = $decode_json["userId"];
		$command = $decode_json["text"];
		$send_to = $decode_json["chat"];
		shorturl($user_id, $command, $send_to);
	}else if($request_type=="client.pressButton"){
		$user_id = $decode_json["userId"];
		$send_to = $decode_json["chat"];
		addUser($send_to, $user_id);
	}
	
?>