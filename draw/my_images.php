<?php
        	require_once('JWT.php');
        	if(isset($_GET["flockEventToken"])){
        			$encryptedtoken = $_GET["flockEventToken"];
        			$token = JWT::decode($encryptedtoken, 'f6831163-fb26-4935-903c-8b3967c02a70');
					$user_id = $token->userId;
        	}else{
        		$user_id = 0;
        	}
		
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <style type="text/css">
        	body{
        		margin: 0px;
        		padding: 0px;
        		background: #F2F2F2;
        		font-family: 'Open Sans', sans-serif;
        	}
        	
				
        	.item{
	        	padding: 8px;
	        	background: #fff;
				border-radius: 4px;
				margin: 12px 8px;	
				box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
				transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        	}
        	.item .link{
			    font-size: 16px;
			    color: #3B5998;
			    padding-bottom: 4px;
        		overflow: hidden;
			    white-space: nowrap;
			    text-overflow: ellipsis;
			    background-color: #FFF;
        	}
        	.item .short_link{
        		font-weight: 350;
        		font-size: 15px;
        		color: #4c9a6e;
        		padding-bottom: 5px;
        	}
        	.item .col{
        		width: 100%;
        		font-size: 14px;
        		padding-top: 8px;
        		color: #707076;
        	}
        	.item .col .col1{
        		width: 100%;
        		float: left;
        		text-align: left;
        	}
        	.item .col .col2{
        		width: 0%;
        		float: right;
        		text-align: right;
        	}
        	.clear{
        		clear: both;
        	}

        	
        	#header{
        		background-color: #FFF;
        		height: 40px;
        		border-bottom: 1px solid #DDDDDD;
        		margin: 0px !important;
        		position: fixed;
        		width: 100%;
        		top: 0;
        	}
        	#header input{
        		width: calc(100% - 50px);
        		height: 40px;
        		padding: 0px 12px;
        		font-size: 16px;
        		font-weight: 100;
        		outline: none;
        		border-width: 0px 0px 0px 0px;
        		margin: 0px !important;
        	}
        	#header input:focus{
        		 outline: none;
        	}
        	.clear_search{
        		width: 40px;
        		height: 40px;
        		padding-top: 10px;
        		line-height: 40px;
        		text-align: center;
        		float: right;
        		cursor: pointer;
        	}
        	#content{
        		margin-top: 48px;
        	}
        	.spinner {
			  width: 40px;
			  height: 40px;
			  background-color: #333;

			  margin: 100px auto 20px auto;
			  -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;
			  animation: sk-rotateplane 1.2s infinite ease-in-out;
			}

			@-webkit-keyframes sk-rotateplane {
			  0% { -webkit-transform: perspective(120px) }
			  50% { -webkit-transform: perspective(120px) rotateY(180deg) }
			  100% { -webkit-transform: perspective(120px) rotateY(180deg)  rotateX(180deg) }
			}

			@keyframes sk-rotateplane {
			  0% { 
			    transform: perspective(120px) rotateX(0deg) rotateY(0deg);
			    -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg) 
			  } 50% { 
			    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
			    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) 
			  } 100% { 
			    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
			    -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
			  }
			}
        </style>
    </head>
    <body>
    <div id="header"><input type="text" id="query" placeholder="Search Images"><div class="clear_search"><img src="http://www.eurocarparts.com/theme/ecp/assets/images/close-icon.png"></div></div>

    	
    	<div id="content">
        
        	<?php
				require_once('config.php');
				if(isset($user_id)){
				$user_id = $user_id;
					
				echo '<div class="list">';
				$find="SELECT * FROM images WHERE uid='$user_id' ORDER BY id DESC";
				$rs = mysqli_query($con, $find);
				while($row = mysqli_fetch_array($rs)) {
		
					echo '<div class="item">
							<div class="link">'.$row["title"].'</div>
							<div class="short_link"><img width="270px" src="'.$row["img"].'"></div> 
							<div class="col">
								<div class="col1">'.$row["ctime"].'</div>
							</div>   
							<div class="clear"></div>			
						</div>';	
				}
				echo '</div>';
		
			}else{
				echo "no";
			}
			
			?>
            
            
        </div>
        
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="https://apps-static.flock.co/js-sdk/0.1.0/flock.js"></script>
   
    	<script type="text/javascript">
    		
			$(".clear_search").click(function() {
			 	$("#query").val("");
			 	$("#query").focus();
			});

$("#query").keyup(function () {
    //split the current value of searchInput
    var data = this.value.toUpperCase().split(" ");
    //create a jquery object of the rows
    var jo = $("#content").find(".item");
    if (this.value == "") {
        jo.show();
        return;
    }
    //hide all the rows
    jo.hide();

    //Recusively filter the jquery object to get results.
    jo.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
                return true;
            }
        }
        return false;
    })
    //show the rows that match.
    .show();
}).focus(function () {
    $(this).unbind('focus');
});




$(".item").click(function() {
	flock.openWidget("https://engineeringbuddy.in/flock/draw/pad/index.php?edit=Yes", "modal", "modal");
});

    	</script>


    </body>
</html>