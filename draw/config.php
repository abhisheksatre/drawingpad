<?php
	$hostname="localhost"; //local server name default localhost
	$username="username";  //mysql username default is root.
	$password="password";       //blank if no password is set for mysql.
	$database="drawing_pad";  //database name which you created

	
	$con = mysqli_connect($hostname, $username, $password, $database);
	if (!$con) {
		die("Connection failed: " . mysqli_connect_error());
	}
	
	date_default_timezone_set("Asia/Kolkata");		
	$vdate=date("d M Y")." " . date("h:i a");
	
?>